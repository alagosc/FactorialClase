package cl.ubb.factorial.test;

import static org.junit.Assert.*;

import org.junit.Test;

import cl.ubb.factorial.Factorial;

public class FactorialTest {

	@Test
	public void FactorialDeCeroEsCero() {
		Factorial fact = new Factorial();
		int resultado = 0;
		
		resultado = fact.calculaFactorial(0);
		
		assertEquals(0, resultado);
		
		
	}

	@Test
	public void FactorialDeUnoEsUno() {
		Factorial fact = new Factorial();
		int resultado = 0;
		
		resultado = fact.calculaFactorial(1);
		
		assertEquals(1, resultado);
		
		
	}
	
	@Test
	public void FactorialDeDosEsDos() {
		Factorial fact = new Factorial();
		int resultado = 0;
		
		resultado = fact.calculaFactorial(2);
		
		assertEquals(2, resultado);
		
		
	}
}
